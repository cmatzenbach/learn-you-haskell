-- this was the first solution I thought of!
initials :: String -> String -> String
initials f l = [head f] ++ [head l]

-- this was the solution they initially wanted
initials2 :: String -> String -> String
initials2 first last = [f] ++ [l]
  where (f:_) = first
        (l:_) = last
