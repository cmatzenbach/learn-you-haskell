myCompare :: (Ord a) => a -> a -> Ordering
myCompare first second
  | first > second = GT
  | first == second = EQ
  | otherwise = LT
