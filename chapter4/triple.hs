fstT :: (a, b, c) -> a
fstT (a, b, c) = a

sndT :: (a, b, c) -> b
sndT (a, b, c) = b

trdT :: (a, b, c) -> c
trdT (_, _, c) = c
