calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [bmi w h | (w, h) <- xs]
  where bmi weight height = weight / height ^ 2

calcBmis2 :: (RealFloat a) => [(a,a)] -> [a]
-- the way that came out of my brain without looking
calcBmis2 xs = [let bmi w h = w / h ^ 2 in bmi x y | (x,y) <- xs ]
-- the author's way
calcBmis2a xs = [bmi | (w, h) <- xs, let bmi = w / h ^ 2]  
