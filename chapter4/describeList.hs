-- case expression
describeList :: [a] -> String
describeList xs = "This list is " ++ case xs of [] -> "empty"
                                                [x] -> "a singleton list"
                                                xs -> "a longer list"

-- pattern matching
describeList2 :: [x] -> String
describeList2 xs = "This list is " ++ check xs
  where check [] = "empty"
        check [x] = "a singleton list"
        check xs = "a longer list"
