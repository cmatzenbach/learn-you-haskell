lucky :: (Integral a) => a -> String
lucky 7 = "Sevense"
lucky x = "Hello world"

factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial a = a * factorial (a - 1)
