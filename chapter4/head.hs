head' :: [a] -> a
head' [] = error "List cannot be empty"
head' (x:_) = x

-- all' :: [a] -> [a]
-- all' [] = "List cannot be empty"
-- all' [15:y] = y
