-- just reports quip for BMI levels
bmiTell :: (RealFloat a) => a -> String
bmiTell bmi
    | bmi <= 18.5 = "You underweight yo"
    | bmi <= 25.0 = "You normal yo"
    | bmi <= 30.0 = "You gettin fat bro"
    | otherwise = "You fat af dude"

-- calculates BMI and send quip
bmiCheck :: (RealFloat a) => a -> a -> String
bmiCheck w h
  | w / h^2 <= 18.5 = "You are underweight my friend"
  | w / h^2 <= 25.0 = "You are normal"
  | w / h^2 <= 30.0 = "You are getting fat"
  | otherwise       = "You are fat af"

-- same as bmiTell but uses name bindings
bmiTellName :: (RealFloat a) => a -> a -> String
bmiTellName w h
    | bmi <= 18.5 = "You're underweight, you emo, you!"  
    | bmi <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | bmi <= 30.0 = "You're fat! Lose some weight, fatty!"  
    | otherwise   = "You're a whale, congratulations!"  
    where bmi = w / h ^ 2

-- same as bmiTell but uses all named bindings
bmiTellName2 :: (RealFloat a) => a -> a -> String
bmiTellName2 w h
    | bmi <= skinny = "You're underweight, you emo, you!"  
    | bmi <= normal = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | bmi <= fat = "You're fat! Lose some weight, fatty!"  
    | otherwise   = "You're a whale, congratulations!"  
    where bmi = w / h ^ 2
          skinny = 18.5
          normal = 25.0
          fat = 30 .0



