replicate' :: (Num a, Ord a) => a -> a -> [a]
replicate' n x
  | n <= 0 = []
  | otherwise = x:replicate' (n-1) x
