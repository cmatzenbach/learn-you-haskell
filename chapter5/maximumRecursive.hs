maximumrec :: (Ord a) => [a] -> a
maximumrec [] = error "Cannot submit empty list"
maximumrec [x] = x
maximumrec (x:xs)
  | x > maxTail = x
  | otherwise = maxTail
  where maxTail = maximumrec xs
