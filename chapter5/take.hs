-- from my head 
take' :: (Num a, Ord a) => a -> [b] -> [b]
take' n xs
  | n <= 0 = []
  | (length xs) <= 1 = []
take' n (x:xs) = x : take' (n-1) xs


-- from the book
take2' :: (Num i, Ord i) => i -> [a] -> [a]
take2' n _
  | n <= 0  = []
take2' _ [] = [] 
take2' n (x:xs) = x : take2' (n-1) xs
