maximumMax :: (Ord a) => [a] -> a
maximumMax [] = error "List cannot be empty"
maximumMax [x] = x
maximumMax (x:xs) = max x (maximumMax xs)
