length' :: [Int] -> Int
length' xs = sum [1 | _ <- xs]

removeUppercase' :: String -> String
removeUppercase' str = [s | s <- str, s `elem` ['a'..'z']]
